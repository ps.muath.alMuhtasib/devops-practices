FROM openjdk:11

ENV DB=h2

EXPOSE 8070

COPY ./target/r*.jar  r.jar

CMD java -jar -Dserver.port=8070 -Dspring.profiles.active=${DB} r*.jar

